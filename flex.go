package main

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"os/exec"
	"path"

	"github.com/docker/docker/api/types/volume"
	"github.com/docker/docker/client"
	"github.com/spf13/cobra"
	"golang.org/x/net/context"
)

const (
	// SUCCESS holds the corresponding string value for FlexVolume.
	SUCCESS = "Success"
	// FAILURE holds the corresponding string value for FlexVolume.
	FAILURE = "Failure"
	// NOTSUPPORTED holds the corresponding string value for FlexVolume.
	NOTSUPPORTED = "Not Supported"
)

// KubeOptions has the options to be passed to the kube pod.
type KubeOptions struct {
	Mountpoint string `json:"mountpoint"`
	Repository string `json:"repository"`
}

// Capabilities holds the caps of a given FlexVolume.
type Capabilities map[string]bool

// FlexResponse holds all fields of a FlexVolume response.
//
// Contents for each field are documented here:
// https://github.com/kubernetes/community/blob/master/contributors/devel/flexvolume.md#driver-output
type FlexResponse struct {
	Status       string       `json:"status,omitempty"`
	Message      string       `json:"message,omitempty"`
	Device       string       `json:"device,omitempty"`
	VolumeName   string       `json:"volumeName,omitempty"`
	Attached     bool         `json:"attached,omitempty"`
	Capabilities Capabilities `json:"capabilities,omitempty"`
}

var initCmd = &cobra.Command{
	Use:   "init",
	Short: "init the cvmfs setup",
	Long: `
Returns the capabilities of this driver, no action is performed.
`,
	SilenceUsage:  true,
	SilenceErrors: true,
	RunE: func(cmd *cobra.Command, args []string) error {
		return flexResponse(
			FlexResponse{
				Status: SUCCESS,
				Capabilities: Capabilities{
					"attach":         false,
					"selinuxRelabel": false,
				},
			})
	},
}

var notSupportedCmd = &cobra.Command{
	Use:           "notsupported",
	Aliases:       []string{"attach", "detach", "getvolumename", "isattached", "mountdevice", "unmountdevice", "waitforattach"},
	Short:         "wrapper for unsupported flexvolume commands",
	SilenceUsage:  true,
	SilenceErrors: true,
	RunE: func(cmd *cobra.Command, args []string) error {
		return flexResponse(FlexResponse{Status: NOTSUPPORTED})
	},
}

var versionCmd = &cobra.Command{
	Use:          "version",
	Short:        "print the versiona and exit",
	SilenceUsage: true,
	Run: func(cmd *cobra.Command, args []string) {
		log.Printf("%v\n", version)
	},
}

var mountCmd = &cobra.Command{
	Use:   "mount <directory> <json>",
	Short: "bind mount the given source dir in target",
	Long: `
Bind mounts the given source directory into target.
`,
	SilenceUsage:  true,
	SilenceErrors: true,
	RunE: func(cmd *cobra.Command, args []string) error {
		if len(args) < 2 {
			err := RepoNotFoundError{}
			return flexError(err)
		}
		var options KubeOptions
		err := json.Unmarshal([]byte(args[1]), &options)
		if err != nil {
			return flexError(err)
		}

		docker, err := dockerClient()
		if err != nil {
			return flexError(err)
		}
		volume, err := docker.VolumeCreate(context.Background(),
			volume.VolumesCreateBody{Driver: "cvmfs", Name: options.Repository})
		if err != nil {
			return flexError(err)
		}

		err = bindMount(volume.Mountpoint, args[0])
		if err != nil {
			return flexError(fmt.Errorf("bind mount failed :: %v :: %v :: %v", args[0], args[1], err))
		}
		return flexResponse(FlexResponse{Status: SUCCESS})
	},
}

var umountCmd = &cobra.Command{
	Use:   "unmount <mountpoint>",
	Short: "unmount the given directory",
	Long: `
Unmounts the given directory.
`,
	SilenceUsage:  true,
	SilenceErrors: true,
	RunE: func(cmd *cobra.Command, args []string) error {
		if len(args) < 1 {
			return flexError(RepoNotFoundError{})
		}

		err := umount(args[0])
		if err != nil {
			return flexError(err)
		}
		return flexResponse(FlexResponse{})
	},
}

func dockerClient() (*client.Client, error) {
	os.Setenv("DOCKER_API_VERSION", dockerVersion)
	return client.NewEnvClient()
}

func bindMount(src string, dest string) error {

	// check if mount directory exists, create if not
	_, err := os.Lstat(dest)
	if err != nil && !os.IsNotExist(err) {
		return fmt.Errorf("failed to check directory :: %v :: %v", dest, err)
	}
	if os.IsNotExist(err) {
		err = os.MkdirAll(dest, os.ModePerm)
		if err != nil {
			return fmt.Errorf("failed to create directory :: %v :: %v", dest, err)
		}
	}

	// bind mount src into dest
	actualSrc := path.Join("/", "rootfs", src)
	cmd := exec.Command("mount", "--bind", actualSrc, dest)
	_, err = cmd.CombinedOutput()
	if err != nil {
		return fmt.Errorf("failed to mount --bind %v :: %v :: %v", src, dest, err)
	}

	return nil
}

func umount(dir string) error {

	// umount the directory
	cmd := exec.Command("umount", dir)
	result, err := cmd.CombinedOutput()
	if err != nil {
		fmt.Printf("failed to unmount :: %v :: %v", err, string(result))
	}
	_ = os.RemoveAll(dir)

	return nil
}

func flexResponse(resp FlexResponse) error {
	result, err := json.Marshal(resp)
	if err != nil {
		return flexError(err)
	}
	fmt.Printf("%v", string(result))
	return err
}

func flexError(err error) error {
	failure := FlexResponse{
		Status:  FAILURE,
		Message: fmt.Sprintf("%v", err),
	}
	result, _ := json.Marshal(failure)
	fmt.Printf("%v", string(result))
	return err
}
